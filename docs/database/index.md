## Database Module

Embedded database for D Programming Language

## Status: Proof of concept

For the time being, don't use for other purposes than fun and development

[Design](design.md)

## Example
```d

import draft.database;

import std.algorithm;
import std.range;
import std.array;
import std.stdio;
import std.conv;

void main(string[] args)
{
    static struct Test
    {
        int a;
        string s;
    }

    static struct Point
    {
        uint a;
        uint b;
        uint c;
    }

    static struct Country
    {
        int a;
        string name;
    }

    auto db = DataBase("testme.db");

    // Creating collections
    auto coll1 = db.collection!Test("collection_name",true);
    auto coll2 = db.collection!Point("Points",true);
    auto coll3 = db.collection!Country("Countries",true);

    // Inserting items (data) into collections 
    coll2.put(Point(1,2,3));

    copy(iota(0,100).map!(x=>Test(1, "Test " ~ x.to!string)).cycle.take(200),coll1);

    int[] smallIntegers = [40,41,42,43];
    short c =1;
    smallIntegers
        .map!(a => Test(c++,"Test ".cycle.take(c).to!string))
        .copy(coll1);

    auto strings = [Country(10,"Poland"), Country(11,"Hungary"), Country(12,"Czechia"), Country(13,"Mars")];

    strings.copy(coll3);

    // Removing (deleting) items

    coll1= db.collection!Test("collection_name");
    coll1.removeItem(Test(1, "Test 55"));

    // Updating items
    coll1.update(Test(1, "Test 56"),Test(1, "Test xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"));

    writeln(db.collection!Test("collection_name"));


    // Filtering items (similar to WHERE clause from SQL)
    auto found = db.collection!Country("Countries").filter!(s => (s.name == "Poland" || s.name.canFind("ar")));
    
    writeln(found);

}

```

## Roadmap

### Version 0.2 (2016-07-01)
* Better support for complex types (arrays, composition, etc.)
* Improve size efficiency
* Add CRC for db items
* Recovery API
* Backup API
* More unit testing
* Better documentation

### Version 0.3 (2016-09-01)
* Introduction of DbReference
* Transaction API
* Initial multi threading support

### Future Development
* Indexes (for lookup speed-up)
* Proper memory management (no gc)
* Locking and multi threading
* Transactions
* Journaling and write ahead logging
* Recovery
* Optimization (db size and execution time)